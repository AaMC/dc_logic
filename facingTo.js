function facingTo(M, N) {
    const facing = [ 'R', 'D', 'L', 'U' ];
    if (!isNaN(M + N) && M > 0 && N > 0) {
        const turns = (N < M) ? (2 * N) - 1 : 2 * (M - 1);
        return facing[ turns % 4 ]
    }
    else return null;
}


$('#btnPrompt').click(function (e) {
    e.preventDefault();
    const mn = [];
    const t = $('#t').val();
    if (isNaN(t)) alert('Numbers only');
    else if (t < 1) alert('At list make T=1');
    else {
        $('#results').removeAttr('hidden');
        $('#resultsArea').empty();
        for (let i = 0; i < t; i++) {
            const M = prompt(`For test #${i + 1} give M value:`);
            const N = prompt(`For test #${i + 1} give N value:`);
            mn.push({ M, N });
        }
        let index = 0;
        for (const matrix of mn) {
            const result = facingTo(matrix.M, matrix.N);
            console.log(result);

            index++;
            if (result) {
                const append = `<p> * In T#${index}, (${matrix.M},${matrix.N}), person is facing to: <b>${result}</b></p>`;
                $('#resultsArea').append(append);
            } else $('#resultsArea').append(`<p> in T#${index}, (${matrix.M},${matrix.N}), <b>invalid values.</b></p>`);
        }
    }



});